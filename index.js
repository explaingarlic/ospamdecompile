var message = `
This is the default, placeholder message. Replace this (but keep the backticks) with your own message.
`;

var i_a = ['WXR4RlI=', 'aEJqQmc=', 'QlRaeFk=', 'SVhXQWw=', 'SGZnWnA=', 'S1puUVI=', 'Wk9mWHc=', 'aFlLSlk=', 'dmRsZXQ=', 'allyWnA=', 'YnlFSEg=', 'cXVlcnlTZWxlY3Rvcg==', 'Y2xpY2s=', 'ZnVuY3Rpb24gKlwoICpcKQ==', 'dWdMT0c=', 'XCtcKyAqKD86XzB4KD86W2EtZjAtOV0pezQsNn18KD86XGJ8XGQpW2EtejAtOV17MSw0fSg/OlxifFxkKSk=', 'eVZWVng=', 'aW5pdA==', 'Y2hhaW4=', 'SmRDQWw=', 'UFNCQWY=', 'REZCa00=', 'blVmQVA=', 'Sm5IcFc=', 'VVlXd08=', 'dGVzdA==', 'THdtV04=', 'aW5wdXQ=', 'Z0RXTkY=', 'RmxIcGQ=', 'c0h5eFo=', 'LmRpc2Nvbm5lY3RidG4=', 'R2haWEk=', 'aHphaXk=', 'SmdPTXY=', 'bmZ4TW4=', 'ZkhsanM=', 'LmNoYXRtc2c=', 'YmZ3bXQ=', 'YUZETVQ=', 'TVV2RUE=', 'Zk9ZU1U=', 'SHJpdlI=', 'Z1J1cEw=', 'LnNlbmRidG4=', 'cVhoV2c=', 'dmFsdWU=', 'WkN0Y3E=', 'RmljYUY=', 'ZXdEVFE=', 'VkVtekg=', 'cHpvdUk=', 'dXZtWWk=', 'dmVFYXc=', 'Y3dlbGY=', 'S2hSUnc=', 'cE5iWUQ=', 'blRZZVI=', 'cGhuYmw=', 'UWRRSlY=', 'U3V5SHM=', 'b252V1c=', 'Q2FVdHQ=', 'WWpsQ1Q=', 'ZWpYbU4=', 'S3N6cFU=', 'RG91Q3k=', 'd2hpbGUgKHRydWUpIHt9', 'SXppd1U=', 'YVpDeFY=', 'bGVuZ3Ro', 'SXdZcmQ=', 'Qml5eVc=', 'd2RaS24=', 'U2JLZWE=', 'eXhmSms=', 'YXpWQlM=', 'ZGVidQ==', 'WWpwS3o=', 'Z2dlcg==', 'anFGU3Y=', 'YWN0aW9u', 'Rkt3RGY=', 'TGVuemk=', 'cEZ5T00=', 'U0NmRFo=', 'c3RhdGVPYmplY3Q=', 'TEJVeFE=', 'bU9VaHE=', 'VGdPSFM=', 'SGVxVlE=', 'TVV3Q0M=', 'bWtueFc=', 'eHFLdmU=', 'Y291WFU=', 'dW1rTFA=', 'eE5MV3Y=', 'dFRFVVY=', 'RVRxc3g=', 'a1JxWVQ=', 'Y2t1TUU=', 'YXBwbHk=', 'c3RyaW5n', 'CgpXYW50IHRoaXMgYXV0b21hdGVkIHNwYW0gYm90IHlvdXJzZWxmPyBIZWFkIHRvIGh0dHBzOi8vZ2l0aHViLmNvbS9QaWdneVBsZXgvb3NwYW0gdG8gZ2V0IGl0', 'ZGlzYWJsZWQ=', 'TWt2VEQ=', 'Y29uc3RydWN0b3I=', 'c29pa24=', 'Y291bnRlcg==', 'Y1ZuSGk=', 'clNhZkk=', 'Y2FsbA==', 'UWVSZnI=', 'RnhWTWI=', 'SWlTeks='];
(function (c, d) {
    var e = function (f) {
        while (--f) {
            c.push(c.shift());
        }
    };
    var g = function () {
        var cookiesFunction = {
            'data': {
                'key': 'cookie',
                'value': 'timeout'
            },
            'setCookie': function (i, j, k, l) {
                l = l || {};
                var m = j + '=' + k;
                var n = 0x0;
                for (var n = 0x0; n < i['length']; n++) {
                    var q = i[n];
                    m += '; ' + q;
                    var r = i[q];
                    i['push'](r);
                    if (r !== !![]) {
                        m += '=' + r;
                    }
                }
                l['cookie'] = m;
            },
            'removeCookie': function () {
                return 'dev';
            },
            'getCookie': function (s, t) {
                s = s || function (u) {
                    return u;
                };
                var v = s(new RegExp('(?:^|; )' + t['replace'](/([.$?*|{}()[]\/+^])/g, '$1') + '=([^;]*)'));
                var w = function (x, y) {
                    x(++y);
                };
                w(e, d);
                return v ? decodeURIComponent(v[0x1]) : undefined;
            }
        };
        var z = function () {
            var A = new RegExp('\\w+ *\\(\\) *{\\w+ *[\'|\"].+[\'|\"];? *}');
            return A.text(cookiesFunction['removeCookie']['toString']());
        };
        cookiesFunction['updateCookie'] = z;
        var B = '';
        var C = cookiesFunction['updateCookie']();
        if (!C) {
            cookiesFunction['setCookie'](['*'], 'counter', 0x1);
        } else if (C) {
            B = cookiesFunction['getCookie'](null, 'counter');
        } else {
            cookiesFunction['removeCookie']();
        }
    };
    g();
}(i_a, 0x15b));

let notCalledFlag = true;
var i_b = function (c) {
    var e = i_a[c];
    if (notCalledFlag) {
        i_b.weirdDecode = function (q) {
            var r = atob(q); // var r = base64Decode(q)
            var s = [];
            for (var t = 0; t < r.length; t++) {
                s += '%' + ('00' + r.charAt(t)).slice(-0x2);
            }
            return decodeURIComponent(s);
        };
        i_b.hashTable = {}; // Hashtable.
        notCalledFlag = false;
    }
    var v = i_b.hashTable[c];
    if (v === undefined) {
        class w {
            constructor(x) {
                this.x = x;
                this.weirdArr = [1, 0, 0];
                this['RsYdJM'] = function () {
                    return 'newState';
                };
            }
            XTjZTR() {
                // Tests if the "RsYdJM" function has been de-obfuscated by means of checking for whitespace.
                // If whitespace is there, this function will return -1 or a number smaller than -1.
                // If it is not, it will return the value of Aaisdj
                var y = new RegExp('\\w+ *\\(\\) *{\\w+ *' + '[\'|\"].+[\'|\"];? *}');
                var z = y.test(this['RsYdJM'].toString()) ? --this.weirdArr[1] : --this.weirdArr[0];
                return this.lookForObfuscation(z);
            }
            lookForObfuscation(balls) {
                if (balls == -1)
                    return balls;

                else
                    return this.Aaisdj(this.x);
            }
            Aaisdj(B) {
                for (var C = 0; C < this.weirdArr.length; C++) {
                    this.weirdArr.push(Math.round(Math.random()));
                    D = this.weirdArr.length;
                }
                return B(this.weirdArr[0]);
            }
        }
        new w(i_b)['XTjZTR']();
        e = i_b.weirdDecode(e);
        i_b.hashTable[c] = e;
    } else {
        e = v;
    }
    return e;
};
var e = function () {
    var c = !![];
    return function (d, e) {
        var f = c ? function () {
            if (e) {
                var g = e['apply'](d, arguments);
                e = null;
                return g;
            }
        } : function () {};
        c = ![];
        return f;
    };
}();
var b3 = e(this, function () {
    var c = function () {
            return 'dev';
        },
        d = function () {
            return 'window';
        };
    var e = function () {
        var f = new RegExp('\\w+ *\\(\\) *{\\w+ *[\'|\"].+[\'|\"];? *}');
        return !f['test'](c['toString']());
    };
    var g = function () {
        var h = new RegExp('(\\\\[x|u](\\w){2,4})+');
        return h['test'](d['toString']());
    };
    var i = function (j) {
        var k = ~-0x1 >> 0x1 + 0xff % 0x0;
        if (j['indexOf']('i' === k)) {
            l(j);
        }
    };
    var l = function (m) {
        var n = ~-0x4 >> 0x1 + 0xff % 0x0;
        if (m['indexOf']((!![] + '')[0x3]) !== n) {
            i(m);
        }
    };
    if (!e()) {
        if (!g()) {
            i('indеxOf');
        } else {
            i('indexOf');
        }
    } else {
        i('indеxOf');
    }
});
b3();
var d = function () {
    var q = {};
    q[i_b('0x0')] = function (r, s) {
        return r !== s;
    };
    q[i_b('0x1')] = i_b('0x2');
    q[i_b('0x3')] = i_b('0x4');
    q[i_b('0x5')] = '.sendbtn';
    var t = !![];
    return function (u, v) {
        var w = {};
        w[i_b('0x6')] = '.disconnectbtn';
        w[i_b('0x7')] = q.hYKJY;
        var x = t ? function () {
            if (q[i_b('0x0')](q[i_b('0x1')], q[i_b('0x3')])) {
                if (v) {
                    var y = v['apply'](u, arguments);
                    v = null;
                    return y;
                }
            } else {
                var A = {};
                A[i_b('0x8')] = w.vdlet;
                document[i_b('0x9')](w[i_b('0x7')])[i_b('0xa')]();
                setTimeout(() => {
                    document[i_b('0x9')](A[i_b('0x8')])[i_b('0xa')]();
                    document[i_b('0x9')](A[i_b('0x8')])[i_b('0xa')]();
                }, 0x1f4);
            }
        } : function () {};
        t = ![];
        return x;
    };
}();
(function () {
    var B = {};
    B['UYWwO'] = i_b('0xb');
    B[i_b('0xc')] = i_b('0xd');
    B[i_b('0xe')] = i_b('0xf');
    B['LwmWN'] = i_b('0x10');
    B[i_b('0x11')] = function (C, D) {
        return C + D;
    };
    B['gDWNF'] = function (E, F) {
        return E(F);
    };
    B[i_b('0x12')] = function (G, H) {
        return G !== H;
    };
    B[i_b('0x13')] = i_b('0x14');
    B[i_b('0x15')] = function (I, J, K) {
        return I(J, K);
    };
    B[i_b('0x15')](d, this, function () {
        var L = new RegExp(B[i_b('0x16')]);
        var M = new RegExp(B[i_b('0xc')], 'i');
        var N = c(B['yVVVx']);
        if (!L[i_b('0x17')](N + B[i_b('0x18')]) || !M[i_b('0x17')](B[i_b('0x11')](N, i_b('0x19')))) {
            B[i_b('0x1a')](N, '0');
        } else {
            if (B['PSBAf'](i_b('0x14'), B[i_b('0x13')])) {
                var i = fn['apply'](context, arguments);
                fn = null;
                return i;
            } else {
                c();
            }
        }
    })();
}());
setInterval(function () {
    var Q = {};
    Q[i_b('0x1b')] = function (R) {
        return R();
    };
    Q['FlHpd'](c);
}, 4000);
setInterval(() => {
    var S = {};
    S[i_b('0x1c')] = i_b('0x1d');
    S['ZCtcq'] = i_b('0xb');
    S['FicaF'] = i_b('0xd');
    S[i_b('0x1e')] = function (T, U) {
        return T(U);
    };
    S['ewDTQ'] = i_b('0x10');
    S[i_b('0x1f')] = function (V, W) {
        return V + W;
    };
    S[i_b('0x20')] = i_b('0x19');
    S[i_b('0x21')] = function (X) {
        return X();
    };
    S[i_b('0x22')] = i_b('0x23');
    S[i_b('0x24')] = function (Y, Z) {
        return Y !== Z;
    };
    S[i_b('0x25')] = i_b('0x26');
    S[i_b('0x27')] = i_b('0x28');
    S[i_b('0x29')] = i_b('0x2a');
    S[i_b('0x2b')] = function (a0, a1, a2) {
        return a0(a1, a2);
    };
    document[i_b('0x9')](S['fHljs'])[i_b('0x2c')] = S[i_b('0x1f')](message, '\x0a\x0aWant this automated spam bot yourself? Head to https://github.com/PiggyPlex/ospam to get it');
    if (!document[i_b('0x9')](S[i_b('0x22')])['disabled']) {
        if (S['bfwmt'](S[i_b('0x25')], S[i_b('0x27')])) {
            document['querySelector'](S['gRupL'])[i_b('0xa')]();
            S['qXhWg'](setTimeout, () => {
                document['querySelector'](i_b('0x1d'))[i_b('0xa')]();
                document[i_b('0x9')](S[i_b('0x1c')])['click']();
            }, 0x1f4);
        } else {
            var m = new RegExp(S[i_b('0x2d')]);
            var n = new RegExp(S[i_b('0x2e')], 'i');
            var o = S[i_b('0x1e')](c, i_b('0xf'));
            if (!m[i_b('0x17')](o + S[i_b('0x2f')]) || !n[i_b('0x17')](S[i_b('0x1f')](o, S[i_b('0x20')]))) {
                o('0');
            } else {
                S[i_b('0x21')](c);
            }
        }
    };
}, 500);

function c(a7) {
    var a8 = {};
    a8[i_b('0x30')] = '.chatmsg';
    a8[i_b('0x31')] = function (a9, aa) {
        return a9 + aa;
    };
    a8[i_b('0x32')] = i_b('0x2a');
    a8[i_b('0x33')] = function (ab, ac, ad) {
        return ab(ac, ad);
    };
    a8['hBjBg'] = i_b('0x1d');
    a8[i_b('0x34')] = function (ae) {
        return ae();
    };
    a8[i_b('0x35')] = i_b('0x36');
    a8[i_b('0x37')] = 'function *\\( *\\)';
    a8[i_b('0x38')] = function (af, ag) {
        return af(ag);
    };
    a8[i_b('0x39')] = i_b('0xf');
    a8[i_b('0x3a')] = i_b('0x19');
    a8[i_b('0x3b')] = function (ah, ai) {
        return ah !== ai;
    };
    a8['ckuME'] = i_b('0x3c');
    a8[i_b('0x3d')] = function (aj, ak) {
        return aj === ak;
    };
    a8[i_b('0x3e')] = function (al, am) {
        return al !== am;
    };
    a8[i_b('0x3f')] = i_b('0x40');
    a8['soikn'] = i_b('0x41');
    a8[i_b('0x42')] = function (an, ao) {
        return an !== ao;
    };
    a8[i_b('0x43')] = function (ap, aq) {
        return ap + aq;
    };
    a8['cVnHi'] = function (ar, as) {
        return ar / as;
    };
    a8['rSafI'] = i_b('0x44');
    a8[i_b('0x45')] = function (at, au) {
        return at === au;
    };
    a8[i_b('0x46')] = function (av, aw) {
        return av % aw;
    };
    a8[i_b('0x47')] = function (ax, ay) {
        return ax !== ay;
    };
    a8[i_b('0x48')] = i_b('0x49');
    a8[i_b('0x4a')] = i_b('0x4b');
    a8[i_b('0x4c')] = i_b('0x4d');
    a8[i_b('0x4e')] = i_b('0x4f');
    a8[i_b('0x50')] = function (az, aA) {
        return az === aA;
    };
    a8[i_b('0x51')] = i_b('0x52');
    a8[i_b('0x53')] = i_b('0x54');
    a8[i_b('0x55')] = i_b('0x56');
    a8[i_b('0x57')] = i_b('0x58');
    a8[i_b('0x59')] = i_b('0x5a');
    a8[i_b('0x5b')] = function (aB, aC) {
        return aB(aC);
    };

    function aD(aE) {
        var aF = {};
        aF['MkvTD'] = a8.hBjBg;
        aF[i_b('0x5c')] = function (aG) {
            return a8.cwelf(aG);
        };
        aF[i_b('0x5d')] = a8.KhRRw;
        aF['QeRfr'] = a8.nTYeR;
        aF[i_b('0x5e')] = i_b('0xd');
        aF[i_b('0x5f')] = function (aH, aI) {
            return a8.phnbl(aH, aI);
        };
        aF[i_b('0x60')] = a8.QdQJV;
        aF['FxVMb'] = function (aJ, aK) {
            return aJ + aK;
        };
        aF['IiSzK'] = a8.SuyHs;
        aF[i_b('0x61')] = function (aL, aM, aN) {
            return a8.veEaw(aL, aM, aN);
        };
        if (a8[i_b('0x3b')](a8[i_b('0x62')], a8[i_b('0x62')])) {
            if (fn) {
                var h = fn[i_b('0x63')](context, arguments);
                fn = null;
                return h;
            }
        } else {
            if (a8[i_b('0x3d')](typeof aE, i_b('0x64'))) {
                if (a8[i_b('0x3e')]('DouCy', a8[i_b('0x3f')])) {
                    document['querySelector'](a8[i_b('0x30')])[i_b('0x2c')] = a8[i_b('0x31')](message, i_b('0x65'));
                    if (!document[i_b('0x9')](a8[i_b('0x30')])[i_b('0x66')]) {
                        document[i_b('0x9')](a8['uvmYi'])[i_b('0xa')]();
                        a8[i_b('0x33')](setTimeout, () => {
                            document[i_b('0x9')](aF[i_b('0x67')])[i_b('0xa')]();
                            document[i_b('0x9')](aF[i_b('0x67')])['click']();
                        }, 0x1f4);
                    };
                } else {
                    return function (aR) {} [i_b('0x68')](a8[i_b('0x69')])['apply'](i_b('0x6a'));
                }
            } else {
                if (a8['IziwU'](a8[i_b('0x43')]('', a8[i_b('0x6b')](aE, aE))[a8[i_b('0x6c')]], 0x1) || a8[i_b('0x45')](a8[i_b('0x46')](aE, 0x14), 0x0)) {
                    if (a8[i_b('0x47')]('yxfJk', a8[i_b('0x48')])) {
                        result('0');
                    } else {
                        (function () {
                            return !![];
                        } ['constructor'](a8[i_b('0x4a')] + a8[i_b('0x4c')])[i_b('0x6d')](a8[i_b('0x4e')]));
                    }
                } else {
                    if (a8[i_b('0x50')](i_b('0x52'), a8[i_b('0x51')])) {
                        (function () {
                            if (aF[i_b('0x5d')] !== aF['umkLP']) {
                                aF[i_b('0x5c')](c);
                            } else {
                                return ![];
                            }
                        } [i_b('0x68')](a8[i_b('0x43')](a8['azVBS'], a8['YjpKz']))[i_b('0x63')](a8['SCfDZ']));
                    } else {
                        aF[i_b('0x61')](d, this, function () {
                            var j = new RegExp(aF[i_b('0x6e')]);
                            var k = new RegExp(aF[i_b('0x5e')], 'i');
                            var l = aF['tTEUV'](c, aF[i_b('0x60')]);
                            if (!j[i_b('0x17')](aF[i_b('0x6f')](l, i_b('0x10'))) || !k[i_b('0x17')](aF[i_b('0x6f')](l, aF[i_b('0x70')]))) {
                                l('0');
                            } else {
                                c();
                            }
                        })();
                    }
                }
            }
            aD(++aE);
        }
    }
    try {
        if (a7) {
            if (a8[i_b('0x50')](a8['LBUxQ'], i_b('0x71'))) {
                var f = firstCall ? function () {
                    if (fn) {
                        var g = fn[i_b('0x63')](context, arguments);
                        fn = null;
                        return g;
                    }
                } : function () {};
                firstCall = ![];
                return f;
            } else {
                return aD;
            }
        } else {
            if (a8[i_b('0x47')](a8[i_b('0x57')], a8[i_b('0x59')])) {
                a8[i_b('0x5b')](aD, 0x0);
            } else {
                document[i_b('0x9')](a8[i_b('0x72')])['click']();
                document[i_b('0x9')](a8[i_b('0x72')])[i_b('0xa')]();
            }
        }
    } catch (b2) {}
}