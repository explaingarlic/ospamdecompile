# ospamdecompile

Came across a "omegle spammer" application. Found it interesting to decompile, and sunk way too much time into it to not atleast put it into source control.

Original project: https://github.com/PiggyPlex/OSpam

Some interesting things in this repo, like the deobfuscation detection using the toString() of a function and testing for whitespace in it.

Might ask around to see if that's intended behaviour in V8.